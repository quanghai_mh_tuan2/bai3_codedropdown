const liDropDownClick = document.querySelectorAll('.drop-down__li--click');
const contentDropDownClick = document.querySelectorAll('.content-drop-down__click');
const liDropDownHover = document.querySelectorAll('.drop-down__li--hover');


// hover vao li hover => an het li click
Array.from(liDropDownHover).forEach((item, index) => {
  item.addEventListener('mouseenter', () => {
    Array.from(contentDropDownClick).forEach((item) => {
      item.classList.add('hidden');
    })
  });
});


// check xem co click nd content drop down li hay khong
window.addEventListener('click', (e) => {
  Array.from(contentDropDownClick).forEach((item, index) => {
    if (item.contains(e.target)) {
      item.classList.toggle('hidden');
    } else {
      Array.from(liDropDownClick).forEach((itemLi, index) => {
        if (itemLi.contains(e.target)) {
          contentDropDownClick[index].classList.toggle('hidden');
        } else {
          contentDropDownClick[index].classList.add('hidden');
        }
      });
    }
  })
});



